﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DnsClient;

namespace MX_Records_Resolver
{
    /// <summary>
    /// Resolves IP addresses from DNS MX records (mail exchange records)
    /// for provided domains and DNS servers.
    /// </summary>
    public class DnsMxResolver
    {
        public DnsMxResolver()
        {
            client = new LookupClient();
        }

        public IEnumerable<DnsMxRecord> ResolveDnsMx(string address, bool isDnsServer = false)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            List<DnsMxRecord> dnsMxRecords = new List<DnsMxRecord>();
            var mxCount = 0;
            try
            {
                if (isDnsServer)
                {
                    string hostName = client.GetHostName(IPAddress.Parse(address));
                    address = hostName; // Not the best approach but it works :D
                }

                var result = client.Query(address, QueryType.MX);
                var mxRecords = result.AllRecords.MxRecords();

                if (result.HasError)
                {
                    dnsMxRecords.Add(
                        new DnsMxRecord(
                            address,
                            result.ErrorMessage
                        )
                    );
                }
                else
                {
                    var error = "";

                    // Check for the number of received MX records.
                    mxCount = mxRecords.Count();

                    // In case of missed MX records, we update error message and continue as more data should be available.
                    if (mxCount != result.Answers.Count)
                    {
                        error = $@"Bad number ({mxCount}/{result.Answers.Count}) of received MX records. Probably redirects occurred.";
                    }

                    // In case no MX records are available, we only add an address and error message.
                    if (mxCount == 0)
                    {
                        error = "No MX records received";
                        dnsMxRecords.Add(
                            new DnsMxRecord(
                                address,
                                error
                            )
                        );
                    }
                    else
                    {
                        foreach (var mxResult in mxRecords)
                        {
                            dnsMxRecords.Add(
                                new DnsMxRecord(
                                    mxResult.DomainName,
                                    string.IsNullOrEmpty(error) ? result.ErrorMessage : error,
                                    mxResult.Exchange,
                                    ProcessExchangeIP(mxResult.Exchange),
                                    mxResult.Preference
                                )
                            );
                        }
                    }
                }
            }
            catch (Exception e)
            {
                dnsMxRecords.Add(
                    new DnsMxRecord(
                        address,
                        e.Message
                    )
                );
            }

            return dnsMxRecords;
        }

        private string ProcessExchangeIP(string exchange)
        {
            var result = client.Query(exchange, QueryType.A, QueryClass.IN);
            if (result.HasError)
                return null;    // TODO: Store error message

            var response = result.Answers.ARecords().First();
            return response.Address.ToString();
        }

        private readonly LookupClient client;
    }
}
