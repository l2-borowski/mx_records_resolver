﻿namespace MX_Records_Resolver
{
    public class DnsMxRecord
    {
        public string Domain { get; }
        public string Error { get; set; }
        public string Exchange { get; }
        public string ExchangeIP { get; }
        public ushort Preference { get; }

        public DnsMxRecord(string domain, string error)
        {
            Domain = domain;
            Error = error;
        }

        public DnsMxRecord(
            string domain,
            string error,
            string exchange,
            string exchangeIP,
            ushort preference)
        {
            Domain = domain;
            Error = error;
            Exchange = exchange;
            ExchangeIP = exchangeIP;
            Preference = preference;
        }
    }

    public class Domain : IAddressToResolve
    {
        IAddressToResolve.AddressType IAddressToResolve.Type => 
            IAddressToResolve.AddressType.Domain;

        public string Address { get; }
        public bool Resolve { get; set; } = true;

        public Domain(string domain)
        {
            Address = domain;
        }
    }

    public class DnsServer : IAddressToResolve
    {
        IAddressToResolve.AddressType IAddressToResolve.Type =>
            IAddressToResolve.AddressType.DnsServer;

        public string Address { get; }
        public bool Resolve { get; set; } = true;

        public DnsServer(string dnsServer)
        {
            Address = dnsServer;
        }
    }
}
