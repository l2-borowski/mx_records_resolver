﻿using Caliburn.Micro;
using MX_Records_Resolver.ViewModels;
using System.Windows;

namespace MX_Records_Resolver
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }
    }
}
