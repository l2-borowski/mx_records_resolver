﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using System.Globalization;

namespace MX_Records_Resolver.ViewModels
{
    /// <summary>
    /// Builds a report based on the given collection and
    /// saves it to a .csv file.
    /// </summary>
    public static class ReportBuilder
    {
        public static void BuildReportToCsv(string path, IEnumerable<DnsMxRecord> dnsMxRecord)
        {
            using (var streamWritter = new StreamWriter(path))
            {
                using (var csvWritter = new CsvWriter(streamWritter, CultureInfo.InvariantCulture))
                {
                    csvWritter.WriteRecords(dnsMxRecord);
                }
            }
        }
    }
}
