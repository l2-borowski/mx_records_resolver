﻿namespace MX_Records_Resolver
{
    public interface IAddressToResolve
    {
        public enum AddressType { Domain, DnsServer };

        public AddressType Type { get; }
        public string Address { get; }
        public bool Resolve { get; set; }
    }
}
