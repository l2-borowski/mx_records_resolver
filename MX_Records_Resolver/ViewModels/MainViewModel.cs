﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Microsoft.Win32;

namespace MX_Records_Resolver.ViewModels
{
    public class MainViewModel : Screen
    {
        public string DomainName
        {
            get => domainName;
            set
            {
                Set(ref domainName, value);
                NotifyOfPropertyChange(nameof(DomainName));
                NotifyOfPropertyChange(nameof(AllowDomainAddition));
            }
        }

        public string DnsServer
        {
            get => dnsServer;
            set
            {
                Set(ref dnsServer, value);
                NotifyOfPropertyChange(nameof(DnsServer));
                NotifyOfPropertyChange(nameof(AllowDnsServerAddition));
            }
        }

        public string FilePath
        {
            get => filePath;
            set => Set(ref filePath, value);
        }

        public string ResolvedStatus
        {
            get => resolvedStatus;
            set => Set(ref resolvedStatus, value);
        }

        public bool IsResolving
        {
            get => isResolving;
            set
            {
                Set(ref isResolving, value);
                NotifyOfPropertyChange(nameof(IsResolving));
                NotifyOfPropertyChange(nameof(AllowBrowseFile));
                NotifyOfPropertyChange(nameof(AllowExportToFile));
                NotifyOfPropertyChange(nameof(AllowClearListToResolve));
                NotifyOfPropertyChange(nameof(AllowResolve));
                NotifyOfPropertyChange(nameof(AllowCancellation));
            }
        }

        public BindableCollection<IAddressToResolve> AddressesToResolve { get; }
        public BindableCollection<DnsMxRecord> DnsMxRecords { get; }

        public bool AllowDomainAddition =>
            !string.IsNullOrEmpty(DomainName);

        public bool AllowDnsServerAddition =>
            !string.IsNullOrEmpty(DnsServer);

        public bool AllowBrowseFile =>
            !IsResolving;

        public bool AllowExportToFile =>
            DnsMxRecords.Count > 0 && !IsResolving;

        public bool AllowClearListToResolve =>
            AddressesToResolve.Count > 0 && !IsResolving;

        public bool AllowResolve =>
            AddressesToResolve.Count > 0 && !IsResolving;

        public bool AllowCancellation =>
            IsResolving;

        public MainViewModel()
        {
            AddressesToResolve = new BindableCollection<IAddressToResolve>();
            DnsMxRecords = new BindableCollection<DnsMxRecord>();
            dnsMxResolver = new DnsMxResolver();
        }
        /// <summary>
        /// Adds new domain to the list.
        /// </summary>
        public void AddDomain()
        {
            if (DomainName != null)
            {
                AddressesToResolve.Add(new Domain(DomainName));
                DomainName = null;
                NotifyOfPropertyChange(nameof(AllowResolve));
                NotifyOfPropertyChange(nameof(AllowClearListToResolve));
            }
        }

        /// <summary>
        /// Adds new DNS server to the list.
        /// </summary>
        public void AddDnsServer()
        {
            if (DnsServer != null)
            {
                AddressesToResolve.Add(new DnsServer(DnsServer));
                DnsServer = null;
                NotifyOfPropertyChange(nameof(AllowResolve));
                NotifyOfPropertyChange(nameof(AllowClearListToResolve));
            }
        }

        /// <summary>
        /// Handles .json file and loads list of domains.
        /// </summary>
        public void AddDomainList()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "json files (*.json)|*.json";
            if (openFileDialog.ShowDialog() == true)
            {
                var path = openFileDialog.FileName;
                var jsonString = File.ReadAllText(path);
                var domainList = JsonSerializer.Deserialize<IEnumerable<string>>(jsonString);

                if (AddressesToResolve != null)
                    AddressesToResolve.Clear();

                foreach (var domain in domainList)
                {
                    AddressesToResolve.Add(new Domain(domain));
                }

                FilePath = path;
                NotifyOfPropertyChange(nameof(AllowResolve));
                NotifyOfPropertyChange(nameof(AllowClearListToResolve));
            }
        }

        /// <summary>
        /// Handles cancellation token for the MX resolving process (in parallel).
        /// </summary>
        public void Cancel()
        {
            tokenSource.Cancel();
        }

        /// <summary>
        /// Clears data from the report table.
        /// </summary>
        public void ClearReportTable()
        {
            DnsMxRecords.Clear();
            NotifyOfPropertyChange(nameof(AllowExportToFile));
        }

        /// <summary>
        /// Clears data from the list to resolve.
        /// </summary>
        public void ClearListToResolve()
        {
            AddressesToResolve.Clear();
            NotifyOfPropertyChange(nameof(AllowResolve));
            NotifyOfPropertyChange(nameof(AllowClearListToResolve));
        }

        /// <summary>
        /// Builds a report based on the DnsMxRecord collection and generates a .csv file.
        /// </summary>
        public void ExportToCsv()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "csv files (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog() == true)
            {
                var path = saveFileDialog.FileName;
                ReportBuilder.BuildReportToCsv(path, DnsMxRecords);
            }
        }

        /// <summary>
        /// Handles MX resolving in parallel.
        /// </summary>
        public async void ResolveMX()
        {
            var addressToResolveCount = 0;
            foreach (var address in AddressesToResolve)
            {
                if (address.Resolve)
                {
                    addressToResolveCount++;
                }
            }

            if (addressToResolveCount <= 0)
                return;

            var i = 0;
            if (DnsMxRecords != null)
                ClearReportTable();

            tokenSource = new CancellationTokenSource();
            ct = tokenSource.Token;
            var task = Task.Run(() =>
            {
                IsResolving = true;

                Parallel.ForEach (AddressesToResolve, 
                    (IAddressToResolve address, ParallelLoopState state) =>
                {
                    // Check if resolving is was cancelled
                    ct.ThrowIfCancellationRequested();

                    if (address.Resolve)
                    {
                        var dnsMx =
                            dnsMxResolver.ResolveDnsMx(
                                address.Address,
                                address.Type ==
                                    IAddressToResolve.AddressType.DnsServer ? true : false
                            );

                        foreach (var resolved in dnsMx)
                        {
                            DnsMxRecords.Add(resolved);
                        }

                        i++;
                        ResolvedStatus = $"{i}/{addressToResolveCount}";

                        if (i >= addressToResolveCount)
                        {
                            IsResolving = false;
                        }
                    }

                    if (ct.IsCancellationRequested)
                    {
                        IsResolving = false;
                        state.Stop();
                    }
                });
            }, tokenSource.Token);

            try
            {
                await task;
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine($"{nameof(OperationCanceledException)} thrown with message: {e.Message}");
            }
            finally
            {
                tokenSource.Dispose();
            }
        }

        private readonly DnsMxResolver dnsMxResolver;
        private CancellationTokenSource tokenSource;
        private CancellationToken ct;
        private string domainName;
        private string dnsServer;
        private string filePath;
        private string resolvedStatus;
        private bool isResolving;
    }
}